var arr = [];

function inKetQua() {
    var number = document.getElementById("nhapSo").value * 1;
    arr.push(number);
    document.getElementById("ketQua").innerHTML = arr;
}

//1. tính tổng số dương
function btn1() {
    var tongSoDuong = 0;

    for (var i = 0; i < arr.length; i++) {
        var current1 = arr[i];

        if (current1 > 0) {
            tongSoDuong += current1;
        }
    }
    document.getElementById("ketQua1").innerHTML = tongSoDuong;
}

//2. đếm số dương
function btn2() {
    var demSoDuong = 0;
    for (var i = 1; i < arr.length; i++) {
        var current1 = arr[i];
        if (current1 > 0) {
            demSoDuong++;
        }
    }
    document.getElementById("ketQua2").innerHTML = demSoDuong;
}

//3. tìm số nhỏ nhất
function btn3() {
    var soNhoNhat = arr[0];
    for (var i = 0; i < arr.length; i++) {
        if (soNhoNhat > arr[i]) {
            soNhoNhat = arr[i];
        }
    }
    document.getElementById("ketQua3").innerHTML = soNhoNhat;
}

//4. tìm số dương nhỏ nhất
function btn4() {
    var soDuongNhoNhat = -1;
    for (var i = 0; i < arr.length; i++) {
        if ((soDuongNhoNhat == -1 || soDuongNhoNhat > arr[i]) && arr[i] > 0) {
            soDuongNhoNhat = arr[i];
        }
    }

    document.getElementById("ketQua4").innerHTML = soDuongNhoNhat;
}

//5. tìm số chẵn cuối cùng
function btn5() {
    var chanCuoiCung = -1;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] % 2 == 0) {
            chanCuoiCung = arr[i];
        }
    }
    document.getElementById("ketQua5").innerHTML = chanCuoiCung;
}

//6. đổi chỗ


//7. sắp xếp tăng dần
function btn7() {
    var sapXep = arr;
    sapXep.sort(function(a, b) {return a - b});
    document.getElementById("ketQua7").innerHTML = sapXep;
}

//8. tìm số nguyên tố đầu tiên
/*hàm kiểm tra số nguyên tố*/
function kiemTraSo(n) {
    var kiemTraSo = true;
    if (n < 2) {
        kiemTraSo = false;
    }
    else if (n == 2) {
        kiemTraSo = true;
    }
    else if (n % 2 == 0) {
        kiemTraSo = false;
    }
    else {
        //lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i < Math.sqrt(n); i += 2) {
            if (n % i == 0) {
                kiemTraSo = false;
                break;
            }
        }
    }
    return kiemTraSo;
}

function btn8() {
    var n = arr;
    var soNguyenToDT = [];
    for (var i = 0; i < arr.length; i++) {
        if (kiemTraSo(arr[i])) {
            soNguyenToDT = arr[i];
            break;
        }
    }
    document.getElementById("ketQua8").innerHTML = soNguyenToDT;
}

//9. đếm số nguyên
/*hàm kiểm tra số nguyên */
function kiemTraSoNguyen(n) {
    var kiemTraSoNguyen = true;
    if (Number.isInteger(n)) {
        kiemTraSoNguyen = true;
    } else {
        kiemTraSoNguyen = false;
    }
    return kiemTraSoNguyen;
}

function btn9() {
    var n = arr;
    var demSoNguyen = [];
    for (var i = 0; i < arr.length; i++) {
        if (kiemTraSoNguyen(arr[i])) {
            demSoNguyen = arr.length;
        }
    }
    document.getElementById("ketQua9").innerHTML = demSoNguyen;
}

//10. so sánh số lượng số âm và dương
function btn10() {
    var tongSoDuong = 0;
    var tongSoAm = 0; 
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            tongSoAm = tongSoAm + 1;
        }
        else if (arr[i] > 0) {
            tongSoDuong = tongSoDuong + 1;
        }
        else if (arr[i] = 0) {
            tongSoDuong = tongSoAm;
        }
    }
    document.getElementById("soDuong").innerHTML = tongSoDuong;
    document.getElementById("soAm").innerHTML = tongSoAm;
}